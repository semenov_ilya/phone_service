package ru.edu.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.edu.service.PhoneService;

import java.util.Date;

@RestController
@RequestMapping(value = "phone")
public class PhoneController {

    private final Logger logger = LogManager.getLogger(PhoneController.class);

    private PhoneService phoneService;

    @Autowired
    public void setDiaryService(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @GetMapping("info")
    public String info() {
        return "PhoneController is here ..." + new Date();
    }

    @PostMapping("telegram/generateCode")
    public String generateTelegramCode(@RequestParam("userId") String userId) {
        logger.info("invoked PhoneController.generateCode");
        return phoneService.generateTelegramCode(userId);
    }
}
