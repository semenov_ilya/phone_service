package ru.edu.service.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import ru.edu.service.PhoneService;
import ru.edu.service.TelegramUtils;

@Component
public class AttachChatCommand extends BotCommand {
    private static final Logger logger = LogManager.getLogger(AttachChatCommand.class);


    @Autowired
    private RestTemplate restTemplate;

    private PhoneService phoneService;

    @Value("${client.auth-service}")
    private String authService;

    @Autowired
    public void setDiaryService(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    public AttachChatCommand() {
        super("reg", "Процессор для привязки User с chatId");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        String userId;
        SendMessage message = null;
        if (strings.length == 1) {
            userId = phoneService.getUserIdByTelegramCode(strings[0]);
            String userLogin = getUserLogin(String.valueOf(chat.getId()));
            if (userLogin == null) {
                if (userId != null) {
                    String url = String.format(authService + "/auth/user/%s/telegram/attach?chatId=%s", userId, chat.getId().toString());
                    ru.edu.dto.User userTemp = restTemplate.exchange(url, HttpMethod.PUT, null, ru.edu.dto.User.class).getBody();
                    message = TelegramUtils.createMessage(String.valueOf(user.getId()), "Телеграмм аккаунт прикреплён к пользователю: " + userTemp.getLogin() + ". Воспользуйтесь командой /help для получения списка команд.");
                } else {
                    message = TelegramUtils.createMessage(String.valueOf(user.getId()), "Пользователь не найден");
                }
            } else {
                message = TelegramUtils.createMessage(String.valueOf(user.getId()), "Данный телеграмм аккаунт невозможно привязать, т.к. уже прикреплён к пользователю: " + userLogin + ".");
            }
        } else {
            message = TelegramUtils.createMessage(String.valueOf(user.getId()), "Введите четырёхзначный код формата /reg xxxx для привязки к пользователю сервиса SDiary");
        }

        try {
            absSender.execute(message);
        } catch (Exception ex) {
            logger.error("Failed to .execute command={} error={}", getCommandIdentifier(), ex.toString(), ex);
        }
    }

    private String getUserLogin(String chatId) {
        logger.info("Method .getUserLogin started chatId={}", chatId);
        final String additionalUrl = "/auth/user/byTelegramChat?chatId=" + chatId;
        String url = authService + additionalUrl;
        ru.edu.dto.user.User user;
        try {
            user = restTemplate.getForObject(url, ru.edu.dto.user.User.class, chatId);
            return user == null ? null : user.getLogin();
        } catch (HttpServerErrorException exception) {
            logger.error("Method .getUserLogin failed chatId={}", chatId);
            return null;
        }
    }
}
